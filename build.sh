NVHPC=0
HPC_SDK_ROOT="/opt/nvidia/hpc_sdk/Linux_x86_64/24.1/"
CUDA=12.3

if [ $# -gt 0 ];
then
	HPC_SDK_ROOT=$1
	NVHPC=1
fi

if [ $# -gt 1 ];
then
	CUDA=$2
	NVHPC=1
fi

if [ -d build ];
then
	rm -rf build
	mkdir build
fi

cd build
if [ $NVHPC -eq 1 ];
then
	echo -e "Building with NVHPC" 
	FC=nvfortran cmake -DNVHPC=On -DCUDA=$CUDA -DNVHPC_DIR=$HPC_SDK_ROOT/cmake/ .. && make
else
	cmake .. && make
fi
cd ..
