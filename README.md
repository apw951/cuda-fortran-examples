### Some simple examples of OpenAcc and CUDA Fortran

Tested with the 24.1 version, and on GTX1080 Ti and H100 gpus, GCC/GFortran 11 nvptx-none offload

- [Programming guide](https://docs.nvidia.com/hpc-sdk/compilers/cuda-fortran-prog-guide/index.html#cf-prog-guide)
- [SDK](https://docs.nvidia.com/hpc-sdk/compilers/index.html) 

### OpenAcc Setup

You may need to install GCC with nvptx-none offload capability.

On Ubuntu 22.04 the following additional install


```
apt install gcc-offload-nvptx
```

was required to resolve the compile error

```
lto-wrapper: fatal error: could not find accel/nvptx-none/mkoffload in /usr/lib/gcc/x86_64-linux-gnu/11/:/usr/lib/gcc/x86_64-linux-gnu/11/:/usr/lib/gcc/x86_64-linux-gnu/:/usr/lib/gcc/x86_64-linux-gnu/11/:/usr/lib/gcc/x86_64-linux-gnu/ (consider using ‘-B’)
```


You should see e.g.

```
$ gfortran -v
Using built-in specs.
COLLECT_GCC=gfortran
COLLECT_LTO_WRAPPER=/usr/lib/gcc/x86_64-linux-gnu/11/lto-wrapper
OFFLOAD_TARGET_NAMES=nvptx-none:amdgcn-amdhsa
...
```